INSERT INTO smallNotesDB.smallNotesUsers (id, email, secret, name, role)
VALUES (1, 'user1@smallnotes.es', '81dc9bdb52d04dc20036dbd8313ed055', 'Carlos García Pérez', 'ROLE_USER');

INSERT INTO smallNotesDB.smallNotesUsers (id, email, secret, name, role)
VALUES (2, 'user2@smallnotes.es', '81dc9bdb52d04dc20036dbd8313ed055', 'Cristóbal García', 'ROLE_USER');

INSERT INTO smallNotesDB.notes (id, title, content, created, owner)
VALUES (1, 'titulo_1', 'content_1', '2010-09-24 12:00:00', 1);

INSERT INTO smallNotesDB.notes (id, title, content, created, owner)
VALUES (2, 'titulo_2', 'content_2', '2010-09-24 13:00:00', 1);

INSERT INTO smallNotesDB.notes (id, title, content, created, owner)
VALUES (3, 'titulo_3', 'content_3', '2010-09-24 13:20:00', 1);

INSERT INTO smallNotesDB.notes (id, title, content, created, owner)
VALUES (4, 'titulo note 4', 'content note 4', '2010-09-24 13:00:00', 2);

INSERT INTO smallNotesDB.links (id, url, noteId)
VALUES (1, 'http://www.carlos-garcia.es', 1);

INSERT INTO smallNotesDB.links (id, url, noteId)
VALUES (2, 'http://www.adictosaltrabajo.com', 1);

INSERT INTO smallNotesDB.links (id, url, noteId)
VALUES (3, 'http://www.autentia.com', 3);

INSERT INTO smallNotesDB.oauth_client_details (client_id, client_secret, authorized_grant_types, authorities, scope, resource_ids, access_token_validity, refresh_token_validity)
VALUES ('smallNotesExternalWebApp_clientID', 'smallNotesExternalWebApp_clientPASSWORD', 'authorization_code', 'ROLE_USER, ROLE_CLIENT', 'read,write', 'api', 500, 50000);

